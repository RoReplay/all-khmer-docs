$(document).ready(function () { 
$("#cancels").click(function(){
    $("#username").val("");
    $("#email").val("");
    $("#title").val("");
    $("#file").val("");
    $("#description").val("");
});

function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
	bs_input_file();
});




 })// end of document ready

 $('#submit').click(function() {
	var userName = $("#username");
	var email = $("#email");
	var title = $("#title");
	var file = $("#file");
	var description = $("#description");
	var rows;
	rows= '<tr>'+
		'<td>'+userName.val()+'</td>'+
		'<td class="myEmail">'+email.val()+'</td>'+
		'<td>'+title.val()+'</td>'+
		'<td class="myFile">'+file.val()+'</td>'+
		'<td class="myDesc">'+description.val()+'</td>'+
		'<td align="center">'+
			'<button type="button" class="btn btn-danger myBtn">Delete</button> &nbsp;&nbsp;'+
			'<button type="button" class="btn btn-warning myBtn">Update</button>'+
		'</td>'+
		'</tr>';
		console.log(file.val());
		$('#tbody').append(rows); 
		userName.val("");
		email.val("");
		title.val("");
		description.val("");
		userName.focus();
})